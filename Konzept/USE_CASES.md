# Mögliche Use 

<details><summary>1. Steuerung der Lüfterklappen in der [maker**space** Bocholt](https://makerspace-bocholt.de/)</summary>

Ein DC_Moter mit einer Untersetzung (z.B auf 8 Umdr/min) treibt die Klappen direkt an.
Zwei Endschalter mit Diode begrenzen den Bewegungswinkel.
Daher wird nur eine zwei adrige Leitung pro Klappe benötigt.

</details>

<details><summary>2. Steuerung von Schliessfächern über Elektromagnete.</summary>

Je ein Elelktromagnet pro Schliesfach kann dieses durch einen Stromimpuls,
von 1 Sekunde, öffnen.
Die Steuerung braucht nur immer ein Elektromagnet ansteuern.
Es werden **nie** mehrere Elektromagnete gleichzeitig angesteuert.

</details>

<details><summary>3. Steuerung von Schliessfächern über Modellbau Servos.</summary>

Die Steuerung kann durch die Änderung des PWMs jeweils ein Modellbau Servo
pro Schliesfach ansteuern.
Die Steuerung braucht nur immer ein Servo ansteuern.
Es werden **nie** mehrere Servos gleichzeitig angesteuert.

</details>

<details><summary>4. Steuerung von beliebigen Anwendungen über Modellbau Servos.</summary>

Die Steuerung kann durch die Änderung des PWMs jedes ein Modellbau Servo
einzeln ansteuern.
Die Steuerung kann alle Servos gleichzeitig individuel ansteuern.

</details>

<details><summary>5. Steuerung der Druckluft von verschiedenen Arbeitsplätzen und Maschienen</summary>

Die Steuerung kann durch anlegen einer Spannung die Magnetventiele diese öffnen
und durch abschalten der Spannnung schließen.
Die Spannungen müssen an den einzelnen Magnetventielen unabhängig von
einander gesteuert werden.

</details>



