Aus der eigentlich nur zur Lüftungsklappenbedienung gedachten Platine ist jetzt eine Universalplatine geworden.

# Konfiguration 1
- 2 DC Motoren 
  - 9 - 48 V 
  - unabhängig und über PWM in beiden Richtungen ansteuerbar.
- 8 Relais die eine externe Spannung jeweils auf eine 2 Polige Schraubklemme schalten kann.
  - Spannung bis 230V AC
  - Strom pro Relais 2A
  - bei AC sind die Freilaufdioden zu entfernen!

# Konfiguration 2
- 1 DC Motoren 
  - 9 - 48 V 
  - PWM in beiden Richtungen ansteuerbar.
- Die zweite Motorsteuerung wird über die 8 Relais auf 2 Polige Schraubklemme geschaltet.
  - 9 - 48 V 
  - immer nur 1 von 8 Motoren kann laufen.(die restlichen sind kurzgeschlossen)
  - PWM in beiden Richtungen ansteuerbar.
  - Freilaufdioden entfernen!



