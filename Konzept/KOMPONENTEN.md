## Mögliche Komponenten

### Relais

#### in der Auswahl

<details>
<summary>
1. Finder Relais Serie 40 und ähnliche
</summary>

- Diese Relais ist gibt es auch von anderen Herstellern mit dem gleichen Foodprint.

- Bei Reichelt als [RT314-12 Leistungsrelais SCHRACK RT1, 12 V DC , 16 A](https://www.reichelt.de/leistungsrelais-schrack-rt1-12-v-dc-16-a-rt314-12-p278453.html?&trstct=pol_1&nbc=1) für 1,60€
    - [Datenblatt](../Konzept/Datenblaetter/RT114-12_DB_EN.pdf)

- Bei Reichelt als [Artikel-Nr.: FIN 40.51.9 12V Steckrelais, 1x UM, 250V/10A, 12V, RM 5,0mm](https://www.reichelt.de/steckrelais-1x-um-250v-10a-12v-rm-5-0mm-fin-40-51-9-12v-p26575.html?&trstct=pol_2&nbc=1) für 4,30€
    - [Datenblatt](../Konzept/Datenblaetter/FIN40%23FIN.pdf)

- Bei Reichelt als [G2RL-11E 12DC Leistungsrelais G2RL, 1 CO, 12 V DC, 16 A](https://www.reichelt.de/leistungsrelais-g2rl-1-co-12-v-dc-16-a-g2rl-11e-12dc-p258324.html?&trstct=pol_14&nbc=1) für 1,45€
    - [Datenblatt](../Konzept/Datenblaetter/G2RL_DB_EN.pdf)
</details>


#### nicht mehr in der Auswahl
<details>
<summary>
2. [**Printrelais NIKKO** N4232A-012-Z1A-P (HG4232A) 12 V-, 1 Wechsler](https://www.pollin.de/p/printrelais-nikko-n4232a-012-z1a-p-hg4232a-12-v-1-wechsler-340546)
</summary>

- Pollin ab 5 Stück 0,60€
    
- Technische Daten:
    - Spulenspannung: 12 V-
    - Spulenwiderstand: 180 Ω
    - Schaltkontakte: 1 Wechsler
    - Schaltstrom: 20 A
    - Schaltspannung: 14 V-
    - Maße (LxBxH): 22,5x16,5x17 mm

</details>

<details>
<summary>
3. [**FUJITSU Relais**, F&T, F1CA024V, 24V, 2 Wechsler 5A 250V~](https://www.pollin.de/p/relais-fujitsu-f-t-f1ca024v-24v-2-wechsler-5a-250v-340953)
</summary>
    
- Pollin 0,75€
- Versiegeltes Relais von Fujitsu für die direkte Printmontage.

- Technische Daten:
    - Spulenspannung: 24 V-
    - Schaltleistung: 24V-/5 A, 250 V~/5 A
    - Kontakt: 2x Wechsler
    - Maße (LxBxH): 29x12x17 mm
</details>

### Servo Ansteuerung

<details>
<summary>
4. [**PCA9685** 16 Kanal 12-bit PWM Servo i²c Modul](https://de.aliexpress.com/item/1005001608315934.html)
</summary>
- AliExpress 3,75€

- Kann direkt 16 Modelbau Servos ansteuern.
- Braucht nur bei Änderungen der Servo Stellung ansteuerung von der CPU sonst vollkommen autark.
- Es können 64 Module über i²c angesprochen werden, das sind 996 Servos.
</details>

### Port Erweiterungen

#### in der Auswahl

<details>
<summary>
5. MCP23017
</summary>

- [**MCP23017-E/SO** I/O-Erweiterung, 16bit, 1,8 - 5V, Seriell, I2C, SO-28](https://www.reichelt.de/i-o-erweiterung-16bit-1-8-5v-seriell-i2c-so-28-mcp23017-e-so-p282743.html) bei Reichelt 1,45

- [E/A-Erweiterung **MCP23017-E/SP**, 16-Kanal I2C, seriell 5MHz, SPDIP 28-Pin](https://de.rs-online.com/web/p/i-o-expander/0403806)
   bei RS 1,71
   
- 16-Bit-I/O-Expander mit serieller Schnittstelle

- Merkmale:
    - Bidirektionaler 16-Bit-Remote-E/A-Anschluss:
    - I/O-Pins standardmäßig auf Eingang
    - Hochgeschwindigkeits-I2C-Schnittstelle (MCP23017):
    - 100kHz
    - 400kHz
    - 1,7MHz
    - Drei Hardware-Adress-Pins ermöglichen bis zu acht Geräte am Bus
    - Konfigurierbare Unterbrechungs-Ausgangspins:
    - Konfigurierbar als Aktiv-Hoch, Aktiv-Niedrig oder Open-Drain
    - INTA und INTB können so konfiguriert werden, dass sie unabhängig voneinander oder gemeinsam operieren
    - Konfigurierbare Unterbrechungsquelle:
    - Interrupt-on-change von konfigurierten Registervorgaben oder Pin-Änderungen
    - Register zur Polaritätsumkehrung, um die Polarität der Eingangsportdaten zu konfigurieren
    - Externer Rücksetzeingang
    - Niedriger Standby-Strom: 1 µA (max.)
    - Betriebsspannung:
    - 1,8V bis 5,5V bei -40°C bis +85°C
    - 2,7V bis 5,5V bei -40°C bis +85°C
    - 4,5V bis 5,5V bei -40°C bis +125°C

- [Arduino Library](https://www.arduino.cc/reference/en/libraries/mcp23017/)

- [Datenblatt](../Konzept/Datenblaetter/MCP23017.pdf)
</details>

#### nicht mehr in der Auswahl

<details>
<summary>
6. PCF8574 Remote 8-Bit I/O Expander for I²C Bus
</summary>

- Als Baustein 1,20€ bei [Reichelt](https://www.reichelt.de/remote-8-bit-i-o-expander-for-i2c-bus-sol-16-pcf-8574-t-p39885.html?&trstct=pos_0&nbc=1)

- Als Modul 0,64€ bei [Aliexpress](https://de.aliexpress.com/item/32713904220.html)

- [Datenblatt](https://www.ti.com/lit/ds/symlink/pcf8574.pdf)

- [Arduino Library](https://www.arduino.cc/reference/en/libraries/pcf8574/)

- [KiCad Library](https://kicad.github.io/symbols/Interface_Expansion)
</details>

<details>
<summary>
7. CJMCU - SX1509 16 kanal I/O ausgang modul und tastatur GPIO spannung ebene led-treiber
</summary>

- 1,77€ bei [AliExpress](https://de.aliexpress.com/item/32705255124.html)

- 8,99 bei [Amazon](https://www.amazon.de/MagiDeal-16-Kanal-Treiber-Tastatur-Arduino/dp/B01GQ6ET0S/)

- [Datasheet](https://cdn.sparkfun.com/datasheets/BreakoutBoards/sx1509.pdf)

- [Arduino Library](https://github.com/sparkfun/SparkFun_SX1509_Arduino_Library)
- [Arduino Library](https://www.arduino.cc/reference/en/libraries/sx1509-io-expander/)

- 16 Bit Portwerweiterung
- kann auf allen Pins PWM ausgeben
</details>
