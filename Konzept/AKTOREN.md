

# Mögliche Aktoren

<details><summary>1. [**Gleichstrom-Getriebemotor** TDY-12, 12 V-, 0,18 A, 8 U/min](https://www.pollin.de/p/gleichstrom-getriebemotor-tdy-12-12-v-0-18-a-8-u-min-310753)</summary>
- Pollin 5,45€

    - Technische Daten:
        - Betriebsspannung: 12 V- (5...15 V-)
        - Stromaufnahme: max. 180 mA bei 12 V-
        - Leistung: max. 4 W
        - Drehzahl: 8 RPM
        - Anschluss: Lötösen
        - Achse mit 3 mm-Bohrung und Gewindebohrung M4
        - Achse (ØxL): 7x13 mm
        - Maße ohne Welle (ØxH): 50x38 mm
</details>




<details><summary>2. [**Servomotor**, analog, 23,5x12,5 mm, Kunststoffgetriebe](https://www.pollin.de/p/servomotor-analog-23-5x12-5-mm-kunststoffgetriebe-820081)</summary>
- Pollin 6,50€

    - Technische Daten:
        - Betriebsspannung: 4,8...6 V-
        - Steuerpuls: 0,7...1,5...2,3 ms
        - Winkelbereich: +/- 70°
        - Stellmoment bei 4,8/6 V-: 1,3/1,5 kg/cm
        - Stellzeit bei 4,8/6 V-: 0,12/0,1 s/60°
        - Gewicht 9 g
        - Gleitlager
        - 3-polige Anschlussleitung, 160 mm
        - Einbaumaße (LxB): 23,5x12,5 mm
        - Gesamtmaße ohne Ruderhorn (LxBxH): 32x12,5x26 mm
</details>

<details><summary>3. [**Türentriegelung**, 12 V-](https://www.pollin.de/p/tuerentriegelung-12-v-340789)</summary>
    - Pollin 12,95€

    - Technische Daten:
        - Betriebsspannung: 12 V-
        - Stromaufnahme: max. 1 A
        - Funktion: Riegel zieht bei Anlegen von 12 V ein
        - Länge Anschlussleitung: 250 mm
        - Riegelmaße (LxBxH): 10x10x10 mm
        - Türöffner-Maße ohne Riegel (LxBxH): 55x40x28 mm

</details> 

<details><summary>4. [**2/2-Wege Magnetventil** mit koaxialer Durchströmung BMV60404M, Messing, EPDM, 12V-](https://www.pollin.de/p/2-2-wege-magnetventil-mit-koaxialer-durchstroemung-bmv60404m-messing-epdm-12v-340898)</summary>
    - Pollin 24,95€

    - Technische Daten:
        - Typ: BMV60404ME28D///100
        - Anschluss Eingang / Ausgang: G 1/4" AG, G 1/8" IG
        - Material Ventilkörper: Messing
        - Material Ventilstößel, Anker: Ferritischer Edelstahl
        - Dichtungsmaterial: EPDM (Ethylen-Propylen-Dien-Kautschuk)
        - Nennweite: 2,8 mm
        - Für Medien: Flüssig und gasförmig (Wasser, Druckluft, CO², Schutzgase in Schweißtechnik MIG, WIG, MAG- Die Beständigkeit gegen weitere Fluide ist - vor Inbetriebnahme entsprechend zu prüfen.
        - Ventiltyp: 2-Wege normal geschlossen (NC)
        - Steuerungsart: direktgesteuert
        - Montageposition: beliebig
        - Betriebsspannung: 12 V-
        - Nennleistung: 7,5 W
        - Max. Schaltdruck: 6,0 bar
        - Isolationsklasse: F (155°C)
        - Einschaltdauer: 100%
        - Elektrischer Anschluss: 6,3 mm Flachstecker, Ventilstecker nach DIN43650/ISO6952, Form B, Industriestandard
        - Gesamtmaße(LxBxH): 65,9x22x40,8 mm
</details>





